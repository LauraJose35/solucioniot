﻿using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Modelos;
using SolucionIOT.BIZ.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SolucionIoT.ClienteWPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Login model;
        public MainWindow()
        {
            InitializeComponent();
            model = this.DataContext as Login;
        }

        private void btnIniciarSesion_Click(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            Usuario u = FactoryManager.UsuarioManager().Login(model.Correo, model.Password);
            this.Cursor = Cursors.Arrow;
            if (u != null)
            {

                //enviar al panel del usurio
                MessageBox.Show($"Bienvenido {u.Nombre}", "Solución IoT", MessageBoxButton.OK, MessageBoxImage.Information);
                PanelUsuario panel = new PanelUsuario(u);
                panel.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Usuario y/o contraseña incorrecta...", "Solución IoT", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
