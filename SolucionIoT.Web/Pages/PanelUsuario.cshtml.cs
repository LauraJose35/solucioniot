using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Modelos;
using SolucionIOT.BIZ.API;

namespace SolucionIoT.Web.Pages
{
    [Authorize]
    public class PanelUsuario : PageModel
    {
        [BindProperty]
        public PanelUsuarioModel DatosPanelUsuario { get; set; }
        [BindProperty]
        public string idEliminar { get; set; }
        public void OnGet(string idUsuario)
        {
            DatosPanelUsuario=new PanelUsuarioModel
            {
                Usuario = FactoryManager.UsuarioManager().BuscarPorId(idUsuario),
                Dispositivos = FactoryManager.DispositivoManager().DispositivosDeUsuarioPorId(idUsuario).ToList()
            };
               
            
        }

        public void onPost()
        {
            if (idEliminar!="")
            {
                FactoryManager.DispositivoManager().Eliminar(idEliminar);
            }
        }
    }
}
