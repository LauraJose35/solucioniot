﻿using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Modelos;
using SolucionIOT.BIZ.API;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SolucionIoTClienteMovil
{
    public partial class MainPage : ContentPage
    {
        Login model;
        public MainPage()
        {
            InitializeComponent();
            model = this.BindingContext as Login;
        }

        private void btnIniciarSesion_Clicked(object sender, EventArgs e)
        {
            Usuario u = FactoryManager.UsuarioManager().Login(model.Correo, model.Password);
            if (u != null)
            {
                //enviar al panel de usuario
                DisplayAlert("Solución IoT", $"Bienvenido {u.Nombre}", "Ok");
                Navigation.PushAsync(new PanelUsuario(u));
            }
            else
            {
                DisplayAlert("Solución IoT", "Usuario y/o contraseña incorrecta...", "Ok");
            }
        }
    }
}
