﻿using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolucionIoT.BIZ
{
    public class LecturaManager : GenericManager<Lectura>, ILecturaManager
    {
        public LecturaManager(IGenericRepository<Lectura> repository) : base(repository)
        {
        }

        public IEnumerable<Lectura> LecturasDelDispositivo(string id)
        {
            //return repository.Query(l => l.IdDispositivo == id);
            return repository.Read.Where(l=>l.IdDispositivo==id);
        }

        public IEnumerable<Lectura> LecturasDelDispositivo(string id, DateTime inicio, DateTime fin)
        {
            //return repository.Query(l => l.IdDispositivo == id && l.FechaHora >= inicio && l.FechaHora <= fin);
            return repository.Read.Where(l => l.IdDispositivo == id && l.FechaHora >= inicio && l.FechaHora <= fin);
        }
    }
}
