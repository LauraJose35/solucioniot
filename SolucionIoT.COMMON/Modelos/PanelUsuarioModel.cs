﻿using SolucionIoT.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionIoT.COMMON.Modelos
{
   public  class PanelUsuarioModel
    {
        public List<Dispositivo> Dispositivos {get; set;}
        public Dispositivo DispositivoSeleccionado { get; set;}
        public Usuario Usuario { get; set; }
        public List<Lectura> LecturasDelDispositivo { get; set; }

        
    }
}


/*
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IotVigilio.BIZ;
using IotVigilio.COMMON.Modelos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
namespace IoteVigilo.Web.Pages
{
    [Authorize]
    public class PanelPrincipalModel : PageModel
    {
        [BindProperty]
        public PanelUsuarioModel DatosPanelUsuario { get; set; }
        public void OnGet(string IdUsuario)
        {
            DatosPanelUsuario = new PanelUsuarioModel();
            DatosPanelUsuario.Usuario = Factory.UsuarioManager().BuscarPorId(IdUsuario);
            DatosPanelUsuario.Proyectos = Factory.ProyectoManager().ListaDeProyectosPorId(IdUsuario).ToList();
        }
    }
}
*/